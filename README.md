# Backend

Para correr primero confirmar presencia de yarn y nodejs

```bash 
node --version
yarn --version
```
Primero instalar dependencias
```bash
yarn install
```
Luego rellenar archivo `.env` con las variables de entorno

```bash
cp .env.example .env
```

Luego correr el servidor

```bash
yarn start
```


