import { software, equipo } from '../models'

//funcion para devolver en json los equipos que trabajaron en un proyecto , conociendo el ID del proyecto.
module.exports = {
    async getEquiposByProyectoId(proyectoId) {

        try {
            const app = await software.findByPk(proyectoId);
            const teams = await app.getEquipos({ joinTableAttributes: [], attributes: ['id', 'nombre'] });

            return !teams ? { statusCode: 404, error: "No hay equipos" } : { statusCode: 200, data: teams };
        }
        catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }

    },
}
