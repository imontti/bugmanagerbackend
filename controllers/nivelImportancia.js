import {
    nivelprioridad,
    ticket,
} from '../models'

module.exports = {
    async getPrioridades() {
        try {
            const results = await nivelprioridad.findAll({ attributes: ['id', 'nombre'] });
            return !results ? { statusCode: 404, error: "No hay tickets" } : { statusCode: 200, data: results };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },
    async updateTicketImportance(ticket_id, nivel) {
        try {
            const ticketToUpdate = await ticket.findByPk(ticket_id);
            if (!ticketToUpdate) {
                return { statusCode: 404, error: "Ticket not found" };
            }

            const updatedTicket = await ticketToUpdate.update({ nivelPrioridad_id: nivel });
            return !updatedTicket
                ? { statusCode: 500, error: "Failed to update ticket importance" }
                : { statusCode: 200, data: { message: "Ticket importance updated successfully" } };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
}
