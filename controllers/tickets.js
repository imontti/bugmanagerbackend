const EmailService = require("../services/EmailService");

import { sequelize } from "../models/index";
import {
    software,
    usuario,
    imagen,
    descripcion,
    ticket,
    rol,
    ticketusuario_aceptado,
    nivelprioridad,
    estado,
    equipo,
    reasignar,
    comentario
    
} from "../models";
import { Op } from "sequelize";
const type_informe = {
    avance: 1,
    final: 2,
};
const stateValue = {
    nuevo: 1,
    asignado: 2,
    progreso: 3,
    finalizado: 4,
    reasignar: 5,
};
module.exports = {
    async getAllTickets() {
        try {
            const tickets = await ticket.findAll({ raw: true });
            return !tickets
                ? { statusCode: 404, error: "No hay tickets" }
                : { statusCode: 200, data: tickets };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },

    async getTicketById(ticketId) {
        try {
            const ticket_byId = await ticket.findByPk(ticketId, {
                attributes: { exclude: ["estado_id", "nivelPrioridad_id", "app_id"] },
                include: [
                    {
                        model: descripcion,
                        attributes: ["detalles", "pasos"],
                    },
                    {
                        model: imagen,
                        attributes: ["url"],
                    },
                    {
                        model: nivelprioridad,
                    },
                    {
                        model: estado,
                        attributes: ["nombre"],
                    },
                    {
                        model: software,
                        attributes: ["id", "nombre"],
                    },
                ],
            });
            return { statusCode: 200, data: ticket_byId };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
    async getTicketsAsignados() {
        try {
            const tickets = await ticket.findAll({
                where: {
                    estado_id: {
                        [Op.or]: [stateValue.asignado, stateValue.progreso]
                    }
                },
                include: [
                    {
                        association: "asignado",
                        attributes: ["id", "nombre", "apellido"],
                    },
                    { model: estado, attributes: ["nombre"] },
                    { model: nivelprioridad, attributes: ["nombre"] },
                    { model: software, attributes: ["id", "nombre"] },
                ],
                attributes: [
                    "id",
                    "titulo",
                    "nombre_reportero",
                    "correo_reportero",
                    "updatedAt",
                ],
            }
            );
            console.log(tickets);
            return !tickets
                ? { statusCode: 404, error: "No hay tickets aceptados por el usuario" }
                : { statusCode: 200, data: tickets };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
    async getTicketsAsignadosDev(devId) {
        try {
            const user = await usuario.findByPk(devId)

            const tickets = await user.getAsignado({
                include: [
                    { model: estado, attributes: ["nombre"] },
                    { model: nivelprioridad, attributes: ["nombre"] },
                    { model: software, attributes: ["id","nombre"] },
                ],
                attributes: [
                    "id",
                    "titulo",
                    "nombre_reportero",
                    "correo_reportero",
                    "updatedAt",
                ],
            });
            return !tickets
                ? { statusCode: 404, error: "No tienes hay tickets Asignados" }
                : { statusCode: 200, data: tickets };
        } catch (error) {
            throw error;
        }
    },
    //Get new tickets
    async getNewTickets() {
        try {
            const tickets = await ticket.findAll({
                where: { estado_id: stateValue.nuevo },
                include: [
                    { model: estado, attributes: ["nombre"] },
                    { model: software, attributes: ["id", "nombre"] },
                ],
                attributes: [
                    "id",
                    "titulo",
                    "nombre_reportero",
                    "correo_reportero",
                    "createdAt",
                ],
            });
            return !tickets
                ? { statusCode: 404, error: "No hay tickets Nuevos" }
                : { statusCode: 200, data: tickets };
        } catch (error) {
            throw error;
        }
    },
    async asignTicket(id, developer_id) {
        // Verificar si el ticket existe
        const ticket_byId = await ticket.findByPk(id); //, { raw: false });
        if (!ticket_byId) return { statusCode: 404, error: "Ticket no encontrado" };
        // Verificar si el usuario existe
        const user = await usuario.findByPk(developer_id);
        if (!user) return { statusCode: 404, error: "Desarrollador no encontrado" };
        try {
            if (await ticket_byId.countAceptado())
                return { statusCode: 404, error: "Ticket ya asignado" };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
        const t = await sequelize.transaction();
        try {
            await ticket_byId.addAceptado(user, { transaction: t });
            await ticket_byId.setEstado(stateValue.aceptado, { transaction: t });
            // await ticket_byId.removeEquipo((await ticket_byId.getEquipos())[0], {
            //     transaction: t,
            // });
            await t.commit();
            await EmailService.sendTicketStatusChangedEmail(ticket_byId.correo_reportero, ticket_byId.nombre_reportero, ticket_byId.titulo, "aceptado");
            return { statusCode: 200, data: ticket_byId };
        } catch (error) {
            await t.rollback();
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
    async obtnerDevTicketsPropuestos(devId) {
        try {
            const user = await usuario.findByPk(devId);
            const teams = await user.getEquipos({ joinTableAttributes: [] });
            console.log(teams);
            var results = [];
            for (const e of teams) {
                const prop = await e.getTickets({
                    include: [descripcion, imagen, estado, nivelprioridad],
                    joinTableAttributes: [],
                });
                for (const t of prop) results.push(t.toJSON());
            }
            return !results
                ? { statusCode: 404, error: "Noy hay tickets propuestos" }
                : { statusCode: 200, data: results };
        } catch (error) {
            throw error;
        }
    },

    async finishTicket(id, user_id) {
        let ticket_byId = null;
        let user = null;
        try {
            ticket_byId = await ticket.findByPk(id);
            if (!ticket_byId)
                return { statusCode: 404, error: "Ticket no encontrado" };
            user = await usuario.findByPk(user_id);
            if (!user)
                return { statusCode: 404, error: "Desarrollador no encontrado" };
            if (!(await ticket_byId.countAceptado()))
                return {
                    statusCode: 404,
                    error:
                        "Ticket no se encuentra asignado, por lo tanto no puede ser finalizado",
                };
            const contAvance = await ticket_byId.countInformes({
                where: { tipo_id: type_informe.avance },
            });
            const contFinal = await ticket_byId.countInformes({
                where: { tipo_id: type_informe.final },
            });
            console.log(contAvance);
            console.log(contFinal);
            if (contAvance == 0 || contFinal != 1)
                return {
                    statusCode: 404,
                    error:
                        "Ticket no puede ser finalizado, dado que no se han subido los informes necesarios",
                };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
        const t = await sequelize.transaction();
        try {
            const user_ticket = (await ticket_byId.getAceptado())[0].id;
            if (user_ticket != user_id)
                return {
                    statusCode: 404,
                    error:
                        "El usuario señalado no está enlazado al ticket que se desea eliminar",
                };
            await ticket_byId.update({
                estado_id: stateValue.finalizado,
                transaction: t,
            });
            await ticket_byId.removeAceptado(user, { transaction: t });
            t.commit();
            await EmailService.sendTicketStatusChangedEmail(ticket_byId.correo_reportero, ticket_byId.nombre_reportero, ticket_byId.titulo, "finalizado");
            return { statusCode: 200, data: ticket_byId };
        } catch (error) {
            t.rollback();
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
    async getAcceptedTicketsByDeveloper(user_id) {
        // En desarrollo
        try {
            const acceptedTickets = await ticketusuario_aceptado.findAll({
                //Consulta base de datos para obtener tickets
                where: { usuario_id: user_id },
                raw: true,
            });
            const tickets = await ticket.findAll({
                //Recopilacion tickets
                where: { id: acceptedTickets.map((ticket) => ticket.ticket_id) },
                raw: true,
            });

            const apps = await software.findAll({
                where: { id: tickets.map((e) => e.app_id) },
                raw: true,
            });
            tickets.forEach((element) => {
                //se recorren todos los tickets y se les asigna nombre de la app
                element.app_name = apps.filter((e) => e.id == element.app_id)[0].nombre;
            });
            return !tickets
                ? {
                    statusCode: 404,
                    error: "No hay tickets aceptados por el desarrollador",
                }
                : { statusCode: 200, data: tickets };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
    async storeNewTicket(
        titulo,
        nombre_reportero,
        correo_reportero,
        app_id,
        estado_id,
        detalles,
        pasos,
        imagenes
    ) {
        try {
            const result = await sequelize.transaction(async (t) => {
                const newTicket = await ticket.create(
                    {
                        titulo: titulo,
                        nombre_reportero: nombre_reportero,
                        correo_reportero: correo_reportero,
                        estado_id: estado_id,
                        app_id: app_id,
                    },
                    { transaction: t }
                );

                await newTicket.createDescripcion(
                    { detalles: detalles, pasos: pasos },
                    { transaction: t }
                );

                for (const image of imagenes) {
                    await imagen.create(
                        {
                            url: image,
                            ticket_id: newTicket.id,
                        },
                        { transaction: t }
                    );
                }
                return newTicket;
            });

            return !result
                ? { statusCode: 404, error: "No se pudo crear el ticket" }
                : { statusCode: 200, data: result };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            throw error; // Lanza el error para ser capturado en el método de ruta
        }
    },

    async assignTicketToEquipo(ticket_id, equipo_id) {
        try {
            const ticketToAssign = await ticket.findByPk(ticket_id);
            if (!ticketToAssign) {
                return { statusCode: 404, error: "Ticket not found" };
            }
            const equipoToAssign = await equipo.findByPk(equipo_id);
            if (!equipoToAssign) {
                return { statusCode: 404, error: "Equipo not found" };
            }

            const existingEquipos = await ticketToAssign.getEquipos();
            let originalEquipoId = null;
            if (existingEquipos.length > 0) {
                originalEquipoId = existingEquipos[0].id;
                await ticketToAssign.removeEquipo(existingEquipos[0]);
            }

            const ticketEquipo = await ticketToAssign.addEquipo(equipoToAssign);
            await EmailService.sendTicketStatusChangedEmail(ticketToAssign.correo_reportero, ticketToAssign.nombre_reportero, ticketToAssign.titulo, `asignado al equipo ${equipo_id}`);

            const message = originalEquipoId
                ? `Reasignado ticket ${ticket_id} desde equipo ${originalEquipoId} a ${equipo_id}`
                : `Asignado ticket ${ticket_id} al equipo ${equipo_id}`;

            return !ticketEquipo
                ? { statusCode: 500, error: "Failed to assign ticket to equipo" }
                : { statusCode: 200, data: { message: message } };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
    },
    async proponerTicketToDev(ticket_id, dev_id, prioridadId) {
        const t = await sequelize.transaction();
        try {
            const result = await ticket.findByPk(ticket_id);
            if (!result)
                return {
                    statusCode: 404,
                    error: "Ticket no existe",
                };

            const dev = await usuario.findByPk(dev_id);
            if (!dev)
                return {
                    statusCode: 404,
                    error: "Desarrolador no existe no Encontrado",
                };
            const prior = await nivelprioridad.findByPk(prioridadId);
            if (!prior)
                return {
                    statusCode: 404,
                    error: "Nivel de Prioridad no existe no Encontrado",
                };
            // const result1 = await result.getPropuesto()
            await result.setAsignado(dev, { transaction: t });
            await result.setNivelprioridad(prior, { transaction: t });
            await result.update({ estado_id: stateValue.asignado }, { transaction: t });
            // await result.setEquipos([], { transaction: t })
            await t.commit();
            await EmailService.sendTicketStatusChangedEmail(result.correo_reportero, result.titulo, result.titulo, "propuesto a desarrollador");
            return !result
                ? { statusCode: 404, error: "No se pudo proponer el ticket" }
                : { statusCode: 200, data: result };
        } catch (error) {
            await t.rollback();
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor",
            };
        }
      },
      async storeNewComment(contenido, usuario_id, ticket_id) {
        try {
            // Verificar que el ticket exista
            const ticketExists = await ticket.findByPk(ticket_id);
            if (!ticketExists) {
                return { statusCode: 404, error: "Ticket no encontrado" };
            }
            
            // Verificar que el usuario exista
            const userExists = await usuario.findByPk(usuario_id);
            if (!userExists) {
                return { statusCode: 404, error: "Usuario no encontrado" };
            }
            // Obtener la aplicación asociada al ticket
        const app = await ticketExists.getSoftware();

        // Obtener los equipos asociados a la aplicación
        const teams = await app.getEquipos({raw: true});

        // Verificar si el usuario pertenece a alguno de los equipos de la aplicación
        const userTeams = await userExists.getEquipos({raw: true});
        const userBelongsToAppTeam = teams.some(team => userTeams.map(t => t.id).includes(team.id));

        if (!userBelongsToAppTeam) {
            return { statusCode: 403, error: "El usuario no pertenece a ninguno de los equipos de la aplicación" };
        }
            
            // Crear el nuevo comentario
            const newComment = await comentario.create({
                contenido,
                usuario_id,
                ticket_id,
            });
            
            return !newComment ? 
                { statusCode: 500, error: "Error al crear el comentario" } : 
                { statusCode: 200, data: `Se ha creado el comentario exitosamente en el ticket ${ticket_id}` };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },
    async reasignarTicket(id, devId) {
        const t = await sequelize.transaction()
        try {
            const bug = await ticket.findByPk(id);
            const state = await estado.findByPk(stateValue.reasignar)
            await bug.setEstado(state, { transaction: t })
            await t.commit()
            await EmailService.sendTicketStatusChangedEmail(bug.correo_reportero, bug.titulo, bug.titulo, "reasignado a desarrollador");
            return !bug
                ? { statusCode: 404, error: `El Ticket ${id} no se pudo reasingar` }
                : { statusCode: 200, data: bug };
        } catch (error) {
            await t.rollback();
            console.error(`ERROR: ${error}`);
            throw error
        }
    },
    async proponerReasignarTicket(id, devId) {
        const t = await sequelize.transaction()
        try {
            const bug = await ticket.findByPk(id);
            const state = await estado.findByPk(stateValue.reasignar)
            const prop = await usuario.findByPk(devId)
            const dev = await bug.getAsignado()
            await reasignar.create({ usuarioId: dev.id, ticketId: bug.id, propuesta: prop.id }, { transaction: t })
            await bug.setEstado(state, { transaction: t })
            await bug.setAsignado(null, { transaction: t })
            await t.commit()
            return !bug
                ? { statusCode: 404, error: `No se pudo procesar la propuesta de reasignacion del ticket ${bug.id} por ${dev.id}` }
                : { statusCode: 201, data: `Se a propuesto exitosamente la reasignación del ticket ${bug.id} al desarrollador ${prop.id}` };
        } catch (error) {
            await t.rollback();
            console.error(`ERROR: ${error}`);
            throw error
        }
    },
};
