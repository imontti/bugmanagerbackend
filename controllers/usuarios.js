const jwt = require('jsonwebtoken');
const secret = 'owoSuperSegurouwu'; // Make sure to keep this key secret and secure

import { equipo, usuario, software, rol, ticket, sequelize } from "../models"

const stateValue = {
    nuevo: 1,
    asignado: 2,
    progreso: 3,
    finalizado: 4,
    rechazado: 5,
};
module.exports = {
    async getDevInSoftware(softwareId) {

        try {
            const app = await software.findByPk(softwareId);
            const equipos = await app.getEquipos({
                include: [
                    {
                        model: usuario,
                        attributes: ['id', 'nombre', 'apellido', 'correo',

                            [sequelize.literal(`(SELECT COUNT(*) FROM ticket WHERE ticket.usuarioId = usuarios.id)`), 'nTickets'],
                            [sequelize.literal(`(SELECT COUNT(*) FROM ticket WHERE ticket.usuarioId = usuarios.id AND ticket.nivelPrioridad_id = 1)`), 'nLow'],
                            [sequelize.literal(`(SELECT COUNT(*) FROM ticket WHERE ticket.usuarioId = usuarios.id AND ticket.nivelPrioridad_id = 2)`), 'nMidium'],
                            [sequelize.literal(`(SELECT COUNT(*) FROM ticket WHERE ticket.usuarioId = usuarios.id AND ticket.nivelPrioridad_id = 3)`), 'nHigh'],
                        ],
                        through: { attributes: [] },
                        include: [{
                            association: 'asignado',
                            attributes: []
                        }]
                    },
                ],
                attributes: ['id', 'nombre'],
                joinTableAttributes: [],
            })

            return !equipos ? { statusCode: 404, error: "No hay equipos" } : { statusCode: 200, data: equipos };
        }
        catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }

    },
    async dataUser(id) {
        try {
            const user = await usuario.findByPk(id, { include: [rol] });
            if (!user) return { statusCode: 404, error: "Usuario no encontrado" };
            let cargaUser = {
                user: user,
                totales: 0,
                asignados: 0,
                progreso: 0,
                finalizados: 0,
            }
            cargaUser.totales = await user.countAsignado();
            cargaUser.asignados = await user.countAsignado({ where: { estado_id: stateValue.asignado } });
            cargaUser.finalizados = await user.countAsignado({ where: { estado_id: stateValue.finalizado } });
            cargaUser.progreso = await user.countAsignado({ where: { estado_id: stateValue.progreso } });
            return { statusCode: 200, data: cargaUser };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },
    async changeMail(id, mail) {
        try {
            const user = await usuario.findByPk(id);
            if (!user) return { statusCode: 404, error: "Usuario no encontrado" };
            await user.update({ correo: mail });
            return { statusCode: 200, data: user };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    },
    async login(correo, password) {
        try {
            const user = await usuario.findOne({ where: { correo: correo, password: password } });
            if (!user) return { statusCode: 404, error: "Usuario no encontrado" };

            const payload = {
                id: user.id,
                correo: user.correo,
                nombre: user.nombre,
                apellido: user.apellido,
                rol: user.rol_id,
            };
            const token = jwt.sign(payload, secret, { expiresIn: '1h' });
            return { statusCode: 200, data: { token } };
        } catch (error) {
            console.error(`ERROR: ${error}`);
            return {
                statusCode: 500,
                error: "Error interno del servidor"
            };
        }
    }
}