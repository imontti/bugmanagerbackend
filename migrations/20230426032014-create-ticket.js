'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ticket', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titulo: {
        allowNull: false,
        type: Sequelize.STRING
      },
      nombre_reportero: {
        allowNull: false,
        type: Sequelize.STRING
      },
      correo_reportero: {
        allowNull: false,
        type: Sequelize.STRING
      },
      estado_id: {
        defaultValue: 1,
        allowNull: true,
        type: Sequelize.INTEGER,
        references:{
          model: 'estado',
          key:  'id'
        },
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
      },
      nivelPrioridad_id: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references:{
          model: 'nivelprioridad',
          key:  'id'
        },
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
      },
      app_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model: 'software',
          key:  'id'
        },
        onDelete : 'CASCADE',
        onUpdate : 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ticket');
  }
};
