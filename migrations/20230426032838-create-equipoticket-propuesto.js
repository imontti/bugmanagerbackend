'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('equipoticket_propuesto', {
      /*id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },*/
      equipo_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        primaryKey: true,
        references:{
          model: 'equipo',
          key:  'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      ticket_id: {
        primaryKey: true,
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model: 'ticket',
          key:  'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('equipoticket_propuesto');
  }
};