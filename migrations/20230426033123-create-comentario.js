'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('comentario', {
      ticket_id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        references:{
          model: 'ticket',
          key:  'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      usuario_id: {
        primaryKey: true,
        allowNull: false,
        type: Sequelize.INTEGER,
        references:{
          model: 'usuario',
          key:  'id'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE',
      },
      contenido: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('comentario');
  }
};