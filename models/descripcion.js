'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class descripcion extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association
      descripcion.belongsTo(models.ticket, { foreignKey: 'ticket_id' })
    }
  }
  descripcion.init({
    ticket_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    detalles: DataTypes.TEXT,
    pasos: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'descripcion',
  });
  return descripcion;
};