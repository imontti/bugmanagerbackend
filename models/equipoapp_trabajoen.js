'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class equipoapp_trabajoen extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //Opcion 2
      //equipoapp_trabajoen.belongsTo(models.equipo, { foreignKey: 'equipo_id'}),
      //equipoapp_trabajoen.belongsTo(models.software, { foreignKey: 'app_id'})
      // define association here
    }
  }
  equipoapp_trabajoen.init({
    equipo_id: {type: DataTypes.INTEGER, primaryKey: true},
    app_id: {type : DataTypes.INTEGER, primaryKey: true}
  }, {
    sequelize,
    modelName: 'equipoapp_trabajoen',
  });
  return equipoapp_trabajoen;
};