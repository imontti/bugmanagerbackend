'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class estado extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      estado.hasMany(models.ticket, {foreignKey: 'estado_id'})
    }
  }
  estado.init({
    nombre: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'estado',
  });
  return estado;
};