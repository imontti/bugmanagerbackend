'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class nivelprioridad extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      nivelprioridad.hasMany(models.ticket, {foreignKey: 'nivelPrioridad_id'})
    }
  }
  nivelprioridad.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'nivelprioridad',
  });
  return nivelprioridad;
};