'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class reasignar extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      reasignar.belongsTo(models.usuario, { as: "proposal", foreignKey: 'propuesta' })
      reasignar.belongsTo(models.ticket)
    }
  }
  reasignar.init({
    usuarioId: {
      primaryKey: true,
      allowNull: false,
      type: DataTypes.INTEGER
    },
    ticketId: {
      primaryKey: true,
      allowNull: false,
      type: DataTypes.INTEGER
    },
    propuesta: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'reasignar',
  });
  return reasignar;
};