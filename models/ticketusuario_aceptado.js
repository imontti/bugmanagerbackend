'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ticketusuario_aceptado extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      //Opcion 2
      // ticketusuario_aceptado.belongsTo(models.ticket, { foreignKey: 'ticket_id'})
      // ticketusuario_aceptado.belongsTo(models.usuario, { foreignKey: 'usuario_id'})
      // define association her
    }
  }
  ticketusuario_aceptado.init({
    usuario_id: DataTypes.INTEGER,
    ticket_id: {
      type: DataTypes.INTEGER,
      primaryKey : true,
      autoIncrement: true
    },
    createdAt: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'ticketusuario_aceptado',
  });
  return ticketusuario_aceptado;
};
