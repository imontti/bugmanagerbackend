'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class usuario extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      //   Asociacion con Ticket "Ready" //
      usuario.belongsToMany(models.ticket, {
        through: 'ticketusuario_aceptado',
        as: 'aceptado',
        foreignKey: 'usuario_id'
      })
      usuario.belongsToMany(models.ticket, {
        through: 'comentario',
        as: 'comment',
        foreignKey: 'usuario_id'
      })
      usuario.hasMany(models.ticket, { as: 'asignado', foreignKey: 'usuarioId' })
      usuario.hasMany(models.informe, { foreignKey: 'usuario_id' })
      /*usuario.belongsToMany(models.ticket, {
        through: "informe",
        as : "report",
        foreignKey: "usuario_id"
      }),*/
      usuario.belongsTo(models.rol, { foreignKey: 'rol_id' })
      /*usuario.belongsToMany(models.equipo, {
        through: 'usuarioequipo_esparte',
        as : 'equipo',
        foreignKey: 'usuario_id'
      }),*/

      // Opcion 2
      //usuario.belongsTo(models.rol, {foreignKey: 'rol_id'}),
      usuario.belongsToMany(models.equipo, {
        through: 'usuarioequipo_esparte',
        foreignKey: 'usuario_id'
      })
      usuario.belongsToMany(models.ticket, {
        through: 'reasignar',
        as: 'reassignment'
      })
      usuario.hasMany(models.reasignar, { as: 'proposal', foreignKey: 'propuesta' })
      /*usuario.belongsToMany(models.ticket, {
        through: 'comentario',
        foreignKey: 'usuario_id'
      })
      usuario.belongsToMany(models.ticket, {
        through: "informe",
        foreignKey: "usuario_id"
      })*/
      // define association here
      // usuario.hasMany(models.ticketusuario_aceptado, { foreignKey: 'usuario_id'})
    }
  }


  usuario.init({
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre: DataTypes.INTEGER,
    apellido: DataTypes.INTEGER,
    correo: DataTypes.INTEGER,
    password: DataTypes.INTEGER,
    rol_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'usuario',
  });
  return usuario;
};