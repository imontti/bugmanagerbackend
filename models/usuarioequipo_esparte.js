'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class usuarioequipo_esParte extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
      // define association here
    }
  }
  usuarioequipo_esParte.init({
    usuario_id: DataTypes.INTEGER,
    equipo_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'usuarioequipo_esparte',
  });
  return usuarioequipo_esParte;
};