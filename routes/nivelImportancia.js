const prioridadController = require("../controllers/nivelImportancia")

module.exports = (app) => {
    app.get('/api/prioridades', async (req, res) => {
        var requestResult;
        try {
            requestResult = await prioridadController.getPrioridades();
        } catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 200 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    }),
    app.put("/api/prioridades", async (req, res) => {
        const { ticket_id, nivel } = req.query;
        if (!ticket_id || !nivel) return res.status(400).send({ error: "ticket_id or nivel missing" });
        if (nivel < 1 || nivel > 3) return res.status(400).send({ error: "nivel must be between 1 and 3" });
        var requestResult;
        try {
            requestResult = await prioridadController.updateTicketImportance(ticket_id, nivel);
        } catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult
            ? res.status(500).send({ error: "Error interno del servidor" })
            : requestResult.statusCode !== 200
            ? res.status(requestResult.statusCode).send(requestResult.error)
            : res.status(200).send(requestResult.data);
    });
}
