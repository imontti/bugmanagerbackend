const softwareController = require('../controllers/software')

module.exports = (app) => {
    app.get('/api/apps', async (req, res) => {
        let requestResult;
        try {
            requestResult = await softwareController.getAllApps();
        }
        catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 200 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    });

    app.get("/api/apps/info", async (req, res) => {
        const { id } = req.query;
        let requestResult;
        try {
            requestResult = await softwareController.infoApp(id) // .lafuncion que creare
        } catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 200 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    });
}