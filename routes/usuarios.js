const usuarioController = require('../controllers/usuarios')

module.exports = (app) => {
    app.get('/api/usuarios', async (req, res) => {
        const { softwareId, devId } = req.query;
        let requestResult;
        try {
            if (softwareId)
                requestResult = await usuarioController.getDevInSoftware(softwareId);
            else if (devId)
                requestResult = await usuarioController.dataUser(devId) // .lafuncion que creare
        } catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult
            ? res.status(500).send({ error: "Error interno del servidor" })
            : requestResult.statusCode !== 200
                ? res.status(requestResult.statusCode).send(requestResult.error)
                : res.status(200).send(requestResult.data);
    });

    app.patch("/api/usuarios", async (req, res) => {
        const { id, mail } = req.body
        var requestResult;
        try {
            requestResult = await usuarioController.changeMail(id, mail) // .lafuncion que creare
        } catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 200 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    });

    //todo: Por ahora los usuarios se crean desde la BDD
    // app.post("/api/usuarios", async (req, res) => {
    //     const { nombre, apellido, correo, password, rol_id } = req.body // Super inseguro XD
    //     var requestResult;
    //     try {
    //         requestResult = await usuarioController.createUser(nombre, apellido, correo, password, rol_id)
    //     } catch (error) {
    //         requestResult = { statusCode: 500, error: "Error interno del servidor" };
    //     }
    //     return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 201 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    // }

    app.post("/api/usuarios/login", async (req, res) => {
        const { correo, password } = req.body
        var requestResult;
        try {
            requestResult = await usuarioController.login(correo, password)
        } catch (error) {
            requestResult = { statusCode: 500, error: "Error interno del servidor" };
        }
        return !requestResult ? res.status(500).send({ error: "Error interno del servidor" }) : requestResult.statusCode !== 200 ? res.status(requestResult.statusCode).send(requestResult.error) : res.status(200).send(requestResult.data);
    });
}