'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
  
    await queryInterface.bulkInsert('nivelprioridad', [
      {
        nombre: 'Baja',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'Media',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        nombre: 'Alta',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
      
      await queryInterface.bulkDelete('nivelprioridad', null, {});  
  }
};
