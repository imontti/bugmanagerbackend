'use strict';

const { DatabaseError } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'ticket',
      [
        {
          titulo: 'Bug pantalla carga',
          estado_id: 1,
          nombre_reportero: 'Andres Dellen',
          correo_reportero: 'adede@gmail.com',
          app_id: 1,
          nivelPrioridad_id: 3,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          titulo: 'Bug al ver la lista de participantes',
          estado_id: 1,
          nombre_reportero: 'Kenyth Amorat',
          correo_reportero: 'kkloamio@gmail.com',
          app_id: 2,
          nivelPrioridad_id: 3,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          titulo: 'Bug en logo de aplicación',
          estado_id: 3,
          nombre_reportero: 'Kito Loundres',
          correo_reportero: 'kihulo@gmail.com',
          app_id: 1,
          nivelPrioridad_id: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          titulo: 'Bug en la busqueda de elementos',
          estado_id: 3,
          nombre_reportero: 'Kike Neira',
          nivelPrioridad_id: 1,
          correo_reportero: 'kikolo@gmail.com',
          app_id: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          titulo: 'Bug pantalla carga',
          estado_id: 2,
          nombre_reportero: 'Andres Dellen',
          correo_reportero: 'adede@gmail.com',
          app_id: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          titulo: 'Bug pantalla carga',
          estado_id: 2,
          nombre_reportero: 'Andres Dellen',
          correo_reportero: 'adede@gmail.com',
          app_id: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ])
  },


  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('ticket', null, {});
  }
};
