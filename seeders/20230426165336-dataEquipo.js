'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'equipo',
      [
        {
          nombre: 'Team G1',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombre: 'Team G2',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombre: 'Team G3',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombre: 'Team G4',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombre: 'Team G5',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nombre: 'Team G6',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ]
    )
  },
  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('equipo', null, {});
  }
};
