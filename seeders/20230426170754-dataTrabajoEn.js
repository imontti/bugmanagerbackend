'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'equipoapp_trabajoen',
      [
        {
          equipo_id: 1,
          app_id: 1,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          equipo_id: 2,
          app_id: 2,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          equipo_id: 3,
          app_id: 3,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          equipo_id: 4,
          app_id: 4,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          equipo_id: 5,
          app_id: 5,
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          equipo_id: 6,
          app_id: 6,
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ]
    )
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
    */
    await queryInterface.bulkDelete('equipoapp_trabajoen', null, {});
  }
};
