'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      'ticketusuario_aceptado', 
      [
        {
          ticket_id: 3,
          usuario_id: 1,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 4,
          usuario_id: 1,
          createdAt: new Date(),
          updatedAt : new Date()
        }/*,
        {
          ticket_id: 6,
          usuario_id: 2,
          createdAt: new Date(),
          updatedAt : new Date()
        }*/
      ]
    )
  },
  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('ticketusuario_aceptado', null, {});
  }
};
