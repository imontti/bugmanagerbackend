'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'comentario', 
      [
        {
          ticket_id: 3,
          usuario_id: 1,
          contenido: "El reporte está siendo evaluado",
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 4,
          usuario_id: 2,
          contenido: "El reporte tiene una gran complejidad",
          createdAt: new Date(),
          updatedAt : new Date()
        }
      ]
    )
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('comentario', null, {});
  }
};
