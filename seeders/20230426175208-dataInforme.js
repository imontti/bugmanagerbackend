'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'informe', 
      [
        {
          ticket_id: 3,
          usuario_id: 1,
          tipo_id: 1,
          titulo: 'Avance de informe: Bug Feo',
          contenido: "El reporte fue encontrado y está siendo evaluado, por lo visto dana la funcionalidad del software ",
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 3,
          usuario_id: 1,
          tipo_id: 2,
          titulo: 'Bug Feo',
          contenido: "El reporte fue encontrado y está siendo evaluado, por lo visto dana la funcionalidad del software ",
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          ticket_id: 4,
          usuario_id: 2,
          tipo_id: 1,
          titulo: ' Informe Avance: Bug entrada',
          contenido: "El reporte mencionado a sido resulto a su totalidad",
          createdAt: new Date(),
          updatedAt : new Date()
        }
      ]
    )
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     * 
     */
    await queryInterface.bulkDelete('informe', null, {});
  }
};
