'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.bulkInsert(
      'imagen', 
      [
        {
          url: 'http:/imagen1',
          ticket_id: 1,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          url: 'http:/imagen1.1',
          ticket_id: 1,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          url: 'http:/imagen2',
          ticket_id: 2,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          url: 'http:/imagen3',
          ticket_id: 3,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          url: 'http:/imagen4',
          ticket_id: 4,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          url: 'http:/imagen5',
          ticket_id: 5,
          createdAt: new Date(),
          updatedAt : new Date()
        },
        {
          url: 'http:/imagen6',
          ticket_id: 6,
          createdAt: new Date(),
          updatedAt : new Date()
        },
      ]
    )
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('imagen', null, {});
  }
};
